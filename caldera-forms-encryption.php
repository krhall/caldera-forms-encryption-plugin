<?php

/**
 * Plugin Name: Caldera Forms Encryption
 * Description: Adds an encryption option for Caldera Forms fields.
 * Author: Compulse, Kevin Hall
 * Author URI: http://compulse.com
 */

if ( !defined('ABSPATH') )
    return;


function cim_caldera_encrypt($data) {
    $key = cim_caldera_get_encryption_key();
    $salt = cim_caldera_get_encryption_salt();
    $cipher = 'AES-128-CBC';
 	$ivLength = openssl_cipher_iv_length($cipher);
 	$iv = openssl_random_pseudo_bytes($ivLength);
    $data .= $salt;
 	$cipherTextRaw = openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
 	$hmac = hash_hmac('sha256', $cipherTextRaw, $key, true);
 	return base64_encode($iv . $hmac . $cipherTextRaw);
}

function cim_caldera_decrypt($data) {
    $key = cim_caldera_get_encryption_key();
    $salt = cim_caldera_get_encryption_salt();
    $cipher = 'AES-128-CBC';
 	$ivLength = openssl_cipher_iv_length($cipher);
 	$content = base64_decode($data);
 	$iv = substr($content, 0, $ivLength);
 	$hmac = substr($content, $ivLength, $sha2len = 32);
 	$cipherTextRaw = substr($content, $ivLength + $sha2len);
 	$calcMac = hash_hmac('sha256', $cipherTextRaw, $key, true);
 	if (hash_equals($hmac, $calcMac)) {
 		$result = openssl_decrypt($cipherTextRaw, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $result = substr($result, 0, strlen($result) - strlen($salt));
        return $result;
 	} else {
 		return 'Unable to decrypt.';
 	}
}

function cim_caldera_get_encryption_key() {
    return 'j1A39J7DvoSAR6oq5mDUH3d5QzhjE8J6';
}

function cim_caldera_get_encryption_salt() {
    return '2vBRNekrRZ3c2n1Hi8Kl6ORkVcYYIcWh';
}


/**
 * Add the encryption checkbox option to the field editor.
 */
function cim_caldera_add_encryption_edit_option($config, $type, $field_id) { ?>
    <?php

    $form = Caldera_Forms_Forms::get_form( $_GET[ Caldera_Forms_Admin::EDIT_KEY ] );

    $encrypted = isset( $form['fields'][ $field_id ]['encrypted'] ) ? $form['fields'][ $field_id ]['encrypted'] : false;

    ?>
    <div class="caldera-config-group entrylist-field">
        <label for="<?php echo esc_attr($field_id); ?>_encrypted"><?php echo esc_html__( 'Encrypted', 'caldera-forms' ); ?></label>
        <div class="caldera-config-field">
            <input type="checkbox" class="field-config field-checkbox" id="<?php echo esc_attr($field_id); ?>_encrypted" name="config[fields][<?php echo esc_attr($field_id); ?>][encrypted]" value="1" <?php if($encrypted){ echo 'checked="checked"'; }else{?>{{#if encrypted}}checked="checked"{{/if}}<?php } ?>>
        </div>
    </div>
<?php }
add_action( 'caldera_forms_field_wrapper_before_field_setup', 'cim_caldera_add_encryption_edit_option', 10, 3 );

/**
 * Encrypt the encrypted fields when they are saved to the database.
 */
function cim_caldera_encrypt_field( $entry, $field, $form, $entry_id ) {
    if ( isset( $field['encrypted'] ) && !!$field['encrypted'] ) {
        $entry = cim_caldera_encrypt( $entry );
    }

    return $entry;
}
add_filter( 'caldera_forms_save_field', 'cim_caldera_encrypt_field', 10, 4 );

/**
 * Remove encrypted field values from the {summary} magic tag.
 */
function cim_caldera_remove_encrypted_fields_from_summary( $value, $field ) {
    if ( isset( $field['encrypted'] ) && !!$field['encrypted'] ) {
        $value = '(Encrypted)';
    }

    return $value;
}
add_filter('caldera_forms_magic_summary_field_value', 'cim_caldera_remove_encrypted_fields_from_summary', 10, 2);

/**
 * Fix the form entry viewer so the encrypted value doesn't stretch the table off the screen.
 */
function cim_caldera_admin_footer() { ?>
    <style>
        div#form-entries-viewer table.cf-table-viewer tbody tr td {
            word-break: break-word;
        }
    </style>
<?php }
add_action('admin_footer', 'cim_caldera_admin_footer');

/**
 * Decrypt encrypted fields when they are being viewed by the "View" button in the entry viewer.
 */
function cim_caldera_decrypt_fields( $data, $entry_id, $form ) {
    if ( current_user_can('manage_options') && did_action( 'wp_ajax_get_entry' ) ) {
        foreach ( $form['fields'] as $field ) {
            if ( isset( $field['encrypted'] ) && !!$field['encrypted'] ) {
                $data['data'][ $field['ID'] ]['view'] = cim_caldera_decrypt( $data['data'][ $field['ID'] ]['view'] );
            }
        }
    }

    return $data;
}
add_filter( 'caldera_forms_get_entry', 'cim_caldera_decrypt_fields', 10, 3 );
